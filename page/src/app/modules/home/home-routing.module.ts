import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './componets/index/index.component';
import { AboutComponent } from './componets/about/about.component';
import { NoteComponent } from './componets/note/note.component';

const routes: Routes = [
  {
    path: '', component: IndexComponent
  },
  {
    path: 'about', component: AboutComponent
  },
  {
    path: 'note', component: NoteComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
