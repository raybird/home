import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { IndexComponent } from './componets/index/index.component';
import { AboutComponent } from './componets/about/about.component';
import { NoteComponent } from './componets/note/note.component';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule
  ],
  declarations: [IndexComponent, AboutComponent, NoteComponent]
})
export class HomeModule { }
